package com.sk.utils;

import com.sk.metric.Student;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * 往kafka中写数据,可以使用这个main函数进行测试
 */
public class KafkaUtil {
    public static final String broker_list = "localhost:9092";
    public static final String topic = "metric";  // kafka topic，Flink 程序中需要和这个统一
    public static volatile AtomicInteger id_incre = new AtomicInteger(0);

    public static void writeToKafka() throws InterruptedException {
        Properties props = new Properties();
        props.put("bootstrap.servers", broker_list);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer"); //key 序列化
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer"); //value 序列化
        KafkaProducer producer = new KafkaProducer<String, String>(props);

//        MetricEvent metric = new MetricEvent();
//        metric.setTimestamp(System.currentTimeMillis());
//        metric.setName("mem");
//        Map<String, String> tags = new HashMap<>();
//        Map<String, Object> fields = new HashMap<>();
//
//        tags.put("cluster", "shik");
//        tags.put("host_ip", "101.147.022.106");
//
//        fields.put("used_percent", 90d);
//        fields.put("max", 27244873d);
//        fields.put("used", 17244873d);
//        fields.put("init", 27244873d);
//
//        metric.setTags(tags);
//        metric.setFields(fields);
//        ProducerRecord record = new ProducerRecord<String, String>(topic, null, null, GsonUtil.toJson(metric));

        Student student = new Student();
        student.setId(new Random().nextInt(5));
        student.setAge(new Random().nextInt(10));
        student.setName("shik test");
        student.setTimeStamp(System.currentTimeMillis());
        ProducerRecord record = new ProducerRecord<String, String>(topic, null, null, GsonUtil.toJson(student));
        producer.send(record);
        System.out.println("发送数据: " + GsonUtil.toJson(student));

        producer.flush();
    }

    public static void main(String[] args) throws InterruptedException {
        while (true) {
            Thread.sleep(300);
            writeToKafka();
        }
    }
}
