package com.sk.kafkaconnector;

import com.sk.metric.Student;
import com.sk.mysqlinout.SinkToMySQL;
import com.sk.schemas.StudentSchema;
import com.sk.utils.ExecutionEnvUtil;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

import static com.sk.utils.KafkaConfigUtil.buildKafkaProps;

/**
 * Desc: Flink 消费 kafka 多个 topic、Pattern 类型的 topic
 */
public class FlinkKafkaConsumerTest5 {

    private static Logger logger = LoggerFactory.getLogger(FlinkKafkaConsumerTest5.class);

    public static void main(String[] args) throws Exception {
        final ParameterTool parameterTool = ExecutionEnvUtil.createParameterTool(args);
        StreamExecutionEnvironment env = ExecutionEnvUtil.prepare(parameterTool);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        Properties props = buildKafkaProps(parameterTool);
        //kafka topic list
        //List<String> topics = Arrays.asList(parameterTool.get("metrics.topic"), parameterTool.get("logs.topic"));
        List<String> topics = Arrays.asList(parameterTool.get("metrics.topic"));
        FlinkKafkaConsumer011<Student> consumer = new FlinkKafkaConsumer011<>(topics, new StudentSchema(), props);
        consumer.setStartFromLatest();
//        consumer.setStartFromEarliest()
        //输入数据
        DataStreamSource<Student> data = env.addSource(consumer);
        //数据处理
         data.assignTimestampsAndWatermarks(new AssignerWithPeriodicWatermarks<Student>() {
             private long currentTimestamp = Long.MIN_VALUE;

             private final long maxTimeLag = 1000L;//生成水印前,允许消息事件时间延迟的时间

             @Nullable
             @Override
             public Watermark getCurrentWatermark() {
                 return new Watermark(currentTimestamp == Long.MIN_VALUE ? Long.MIN_VALUE : currentTimestamp - maxTimeLag);
             }

             @Override
             public long extractTimestamp(Student element, long previousElementTimestamp) {
                 long timestamp = element.getTimeStamp();
                 currentTimestamp = Math.max(timestamp, currentTimestamp);
                 return timestamp;
             }
         }).flatMap(new FlatMapFunction<Student, Tuple3<Integer,Integer,String>>() {
            @Override
            public void flatMap(Student student, Collector<Tuple3<Integer,Integer,String>> out) throws Exception {
                //logger.info("FlinkKafkaConsumerTest2#flatMap {}",student.toString());
                if(Objects.nonNull(student) && student.getId() % 2 == 0) {
                    out.collect(Tuple3.of(student.getId(),student.getAge(),student.getName()));
                }
            }
        })
       .keyBy(0).timeWindow(Time.seconds(10),Time.seconds(3))
       .process(new ProcessWindowFunction<Tuple3<Integer,Integer,String>, Tuple4<Integer, Integer, String,String>, Tuple, TimeWindow>() {

                     @Override
                     public void process(Tuple tuple, Context context, Iterable<Tuple3<Integer, Integer, String>> iterable, Collector<Tuple4<Integer, Integer, String, String>> collector) throws Exception {
                         for(Tuple3<Integer,Integer,String> element : iterable){
                             logger.info("FlinkKafkaConsumerTest2#process {}",element.toString());
                             Tuple4<Integer,Integer,String,String> result = Tuple4.of(element.f0,element.f1,element.f2,element.f1+element.f2+element.f0);
                             collector.collect(result);
                         }
                     }
                 })
                 .setParallelism(2)
                 .addSink(new SinkToMySQL());

        //数据输出
        //data.print();
        env.execute("flink kafka connector test");
    }
}
