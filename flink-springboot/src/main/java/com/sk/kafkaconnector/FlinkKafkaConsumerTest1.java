package com.sk.kafkaconnector;

import com.sk.metric.MetricEvent;
import com.sk.metric.Student;
import com.sk.schemas.MetricSchema;
import com.sk.schemas.StudentSchema;
import com.sk.utils.ExecutionEnvUtil;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

import static com.sk.utils.KafkaConfigUtil.buildKafkaProps;

/**
 * Desc: Flink 消费 kafka 多个 topic、Pattern 类型的 topic
 */
public class FlinkKafkaConsumerTest1 {

    private static Logger logger = LoggerFactory.getLogger(FlinkKafkaConsumerTest1.class);

    public static void main(String[] args) throws Exception {
        final ParameterTool parameterTool = ExecutionEnvUtil.createParameterTool(args);
        StreamExecutionEnvironment env = ExecutionEnvUtil.prepare(parameterTool);
        Properties props = buildKafkaProps(parameterTool);
        //kafka topic list
        //List<String> topics = Arrays.asList(parameterTool.get("metrics.topic"), parameterTool.get("logs.topic"));
        List<String> topics = Arrays.asList(parameterTool.get("metrics.topic"));
        //FlinkKafkaConsumer011<MetricEvent> consumer = new FlinkKafkaConsumer011<>(topics, new MetricSchema(), props);
        FlinkKafkaConsumer011<Student> consumer = new FlinkKafkaConsumer011<>(topics, new StudentSchema(), props);
        consumer.setStartFromLatest();
//        consumer.setStartFromEarliest()
        //输入数据
        DataStreamSource<Student> data = env.addSource(consumer);
        //数据处理
         data.flatMap(new FlatMapFunction<Student, Student>() {
            @Override
            public void flatMap(Student student, Collector<Student> collector) throws Exception {
                logger.info("FlinkKafkaConsumerTest1#flatMap {}",student.toString());
                if(Objects.nonNull(student) && student.getAge() % 2 == 0) {
                    collector.collect(student);
                }
            }
        }).addSink(new KafkaSink1());

        //数据输出
        //data.print();

        env.execute("flink kafka connector test");
    }
}
