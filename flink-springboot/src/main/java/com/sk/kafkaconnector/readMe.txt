这是一个 flink+ kafka 生产者+消费者模式的demo：



启动方式：
1，开启 zookeeper：   cmd回车：  zkServer
2. 开启 kafka-server：
新建cmd窗口:
      cd E:\softLibrary\kafka_2.11-2.1.1\
     .\bin\windows\kafka-server-start.bat .\config\server.properties

3.  先启动消费者：
FlinkKafkaConsumerTest1的 main方法：
4.  再启动生产者：
FlinkKafkaProducerTest1的 main方法：

就可以看到效果：如下：
2> MetricEvent{name='mem', timestamp=1592642704878, fields={init=2.7244873E7, max=2.7244873E7, used_percent=90.0, used=1.7244873E7}, tags={cluster=shik, host_ip=101.147.022.106}}
2> MetricEvent{name='mem', timestamp=1592642704878, fields={init=2.7244873E7, max=2.7244873E7, used_percent=90.0, used=1.7244873E7}, tags={cluster=shik, host_ip=101.147.022.106}}
2> MetricEvent{name='mem', timestamp=1592642705187, fields={init=2.7244873E7, max=2.7244873E7, used_percent=90.0, used=1.7244873E7}, tags={cluster=shik, host_ip=101.147.022.106}}
2> MetricEvent{name='mem', timestamp=1592642705187, fields={init=2.7244873E7, max=2.7244873E7, used_percent=90.0, used=1.7244873E7}, tags={cluster=shik, host_ip=101.147.022.106}}
等等。。。