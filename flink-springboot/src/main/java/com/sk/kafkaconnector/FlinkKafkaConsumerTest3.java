package com.sk.kafkaconnector;

import com.sk.metric.Student;
import com.sk.schemas.StudentSchema;
import com.sk.utils.ExecutionEnvUtil;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.Context;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

import static com.sk.utils.KafkaConfigUtil.buildKafkaProps;

/**
 * Desc: Flink 消费 kafka 多个 topic、Pattern 类型的 topic
 */
public class FlinkKafkaConsumerTest3 {

    private static Logger logger = LoggerFactory.getLogger(FlinkKafkaConsumerTest3.class);

    public static void main(String[] args) throws Exception {
        final ParameterTool parameterTool = ExecutionEnvUtil.createParameterTool(args);
        StreamExecutionEnvironment env = ExecutionEnvUtil.prepare(parameterTool);
        Properties props = buildKafkaProps(parameterTool);
        //kafka topic list
        //List<String> topics = Arrays.asList(parameterTool.get("metrics.topic"), parameterTool.get("logs.topic"));
        List<String> topics = Arrays.asList(parameterTool.get("metrics.topic"));
        //FlinkKafkaConsumer011<MetricEvent> consumer = new FlinkKafkaConsumer011<>(topics, new MetricSchema(), props);
        FlinkKafkaConsumer011<Student> consumer = new FlinkKafkaConsumer011<>(topics, new StudentSchema(), props);
        consumer.setStartFromLatest();
//        consumer.setStartFromEarliest()
        //输入数据
        DataStreamSource<Student> data = env.addSource(consumer);
        //数据处理
         data.flatMap(new FlatMapFunction<Student, Tuple3<Integer,Integer,String>>() {
            @Override
            public void flatMap(Student student, Collector<Tuple3<Integer,Integer,String>> out) throws Exception {
                logger.info("FlinkKafkaConsumerTest2#flatMap {}",student.toString());
                if(Objects.nonNull(student) && student.getId() % 2 == 0) {
                    out.collect(Tuple3.of(student.getId(),student.getAge(),student.getName()));
                }
            }
        })
       .keyBy(0).sum(1)
                 .setParallelism(2)
       .print();

        //数据输出: 数据的输入是一个无限流,他是一个根据key 做分组, 第二个数做 累计sum()
        //data.print();

        env.execute("flink kafka connector test");
    }
}
