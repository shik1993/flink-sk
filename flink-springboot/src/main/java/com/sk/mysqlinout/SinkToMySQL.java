package com.sk.mysqlinout;

import com.sk.kafkaconnector.FlinkKafkaConsumerTest4;
import com.sk.metric.Student;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

/**
 * Desc: 数据批量 sink 数据到 mysql
 */
public class SinkToMySQL extends RichSinkFunction<Tuple4<Integer, Integer, String, String>> {
    private static Logger logger = LoggerFactory.getLogger(FlinkKafkaConsumerTest4.class);
    PreparedStatement ps;
    BasicDataSource dataSource;
    private Connection connection;

    /**
     * open() 方法中建立连接，这样不用每次 invoke 的时候都要建立连接和释放连接
     *
     * @param parameters
     * @throws Exception
     */
    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        dataSource = new BasicDataSource();
        connection = getConnection(dataSource);
        String sql = "insert into credit_request(gw_order_no,user_id) values(?, ?);";
        if (connection != null) {
            ps = this.connection.prepareStatement(sql);
        }
    }

    @Override
    public void close() throws Exception {
        super.close();
        //关闭连接和释放资源
        if (connection != null) {
            connection.close();
        }
        if (ps != null) {
            ps.close();
        }
    }

    /**
     * 每条数据的插入都要调用一次 invoke() 方法
     *
     * @param value
     * @param context
     * @throws Exception
     */
    @Override
    public void invoke(Tuple4<Integer, Integer, String, String> value, Context context) throws Exception {
        if (ps == null) {
            return;
        }
        //遍历数据集合
            ps.setString(1, value.f2);
            ps.setString(2, value.f3);
            ps.addBatch();

        int[] count = ps.executeBatch();//批量后执行
        logger.info("成功了插入了 {} 行数据", count.length);
    }


    private static Connection getConnection(BasicDataSource dataSource) {
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        //注意，替换成自己本地的 mysql 数据库地址和用户名、密码
        dataSource.setUrl("jdbc:mysql://localhost:3306/shik");
        dataSource.setUsername("root");
        dataSource.setPassword("020332kk");
        //设置连接池的一些参数
        dataSource.setInitialSize(10);
        dataSource.setMaxTotal(50);
        dataSource.setMinIdle(2);

        Connection con = null;
        try {
            con = dataSource.getConnection();
            logger.info("创建连接池：{}", con);
        } catch (Exception e) {
            logger.error("-----------mysql get connection has exception , msg = {}", e.getMessage());
        }
        return con;
    }
}
