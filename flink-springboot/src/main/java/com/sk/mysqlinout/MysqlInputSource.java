package com.sk.mysqlinout;

import com.sk.mysqlinout.dto.CreditRequest;
import com.sk.utils.MySQLUtil;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.source.RichParallelSourceFunction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MysqlInputSource extends RichParallelSourceFunction<CreditRequest> {

    private PreparedStatement ps;
    private Connection connection;

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        connection = MySQLUtil.getConnection("com.mysql.jdbc.Driver",
                "jdbc:mysql://localhost:3306/shik?useUnicode=true&characterEncoding=UTF-8",
                "root",
                "020332kk");
        String sql = "select * from credit_request;";
        ps = this.connection.prepareStatement(sql);
    }

    @Override
    public void close() throws Exception {
        super.close();
        if (connection != null) { //关闭连接和释放资源
            connection.close();
        }
        if (ps != null) {
            ps.close();
        }
    }


    @Override
    public void run(SourceContext<CreditRequest> sourceContext) throws Exception {
        ResultSet resultSet = ps.executeQuery();
        while (resultSet.next()) {
            System.out.println("MysqlInputSource#run "
                    + resultSet.getInt("id") +"\t"
                    + resultSet.getString("user_id"));
             CreditRequest request = CreditRequest.newInstance();
             request.setId(resultSet.getLong("id"));
             request.setUserId(resultSet.getString("user_id"));
             sourceContext.collect(request);
        }
    }

    @Override
    public void cancel() {

    }
}
