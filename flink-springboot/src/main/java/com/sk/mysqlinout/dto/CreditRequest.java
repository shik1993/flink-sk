package com.sk.mysqlinout.dto;

import java.util.Date;

public class CreditRequest extends Entity {

    /**
     * 网关请求流水
     */
    private String gwOrderNo;

    /**
     * 请求参数
     */
    private String requestJson;

    /**
     * 请求查询类型（AccessEnum）
     */
    private String accessType;

    /**
     * 自增id
     */
    private Long id;
    /**
     * userId
     */
    private String userId;
    /**
     * 通道编码
     */
    private String tppCode;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;

    public CreditRequest() {
        super();
    }

    public CreditRequest(Long id, String userId) {
        this.id = id;
        this.userId = userId;
    }

    public static CreditRequest newInstance(){
        return new CreditRequest();
    }
    public String getGwOrderNo() {
        return gwOrderNo;
    }

    public void setGwOrderNo(String gwOrderNo) {
        this.gwOrderNo = gwOrderNo;
    }

    public String getRequestJson() {
        return requestJson;
    }

    public void setRequestJson(String requestJson) {
        this.requestJson = requestJson;
    }

    public String getAccessType() {
        return accessType;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTppCode() {
        return tppCode;
    }

    public void setTppCode(String tppCode) {
        this.tppCode = tppCode;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CreditRequest{");
        sb.append("gwOrderNo='").append(gwOrderNo).append('\'');
        sb.append(", requestJson='").append(requestJson).append('\'');
        sb.append(", accessType='").append(accessType).append('\'');
        sb.append(", id=").append(id);
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", tppCode='").append(tppCode).append('\'');
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append('}');
        return sb.toString();
    }
}